# Usage instruction

- Install module, update the VERSION file in your composer root & visit admin/reports/status
- On every release, update the VERSION file & commit 

## Sample bash to automate VERSION file creation in build process

````bash
#!/bin/bash
versionFile=VERSION
version=$(git rev-parse --abbrev-ref HEAD)
if [ $version = 'HEAD' ]; then
  version=$(git describe --tags --abbrev=0)
fi
if [ -e $versionFile ]; then
  rm $versionFile
fi
if [ ! -e $versionFile ]; then
  echo $version >> $versionFile
fi
````
