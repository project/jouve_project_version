<?php

namespace Drupal\jouve_project_version;

use Drupal\Core\Cache\CacheBackendInterface;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Version service.
 *
 * @package Drupal\jouve_project_version
 */
class Service implements ServiceInterface {

  const CID = 'jouve_project_version';

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs an Example object.
   *
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cache_factory
   *   The cache factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * {@inheritDoc}
   */
  public function getVersion(): ?string {
    if ($cache = $this->cache->get(self::CID)) {
      return $cache->data;
    }
    $version_file = $this->getVersionFile();
    if ($version_file ?? NULL) {
      $version = preg_replace("/\r|\n/", "", reset($version_file));
      $this->cache->set(self::CID, $version);
      return $version;
    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getVersionFile(): ?array {
    $fs = new Filesystem();
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $composerRoot = $drupalFinder->getComposerRoot();
    $version_file = $composerRoot . '/VERSION';
    if ($fs->exists($version_file)) {
      return file($version_file);
    }
    return NULL;
  }


}
