<?php


namespace Drupal\jouve_project_version;

/**
 * Version Service Interface.
 *
 * @package Drupal\jouve_project_version
 */
interface ServiceInterface {

  /**
   * Get the current version.
   */
  public function getVersion(): ?string;

  /**
   * Get the version file.
   */
  public function getVersionFile(): ?array;
}
